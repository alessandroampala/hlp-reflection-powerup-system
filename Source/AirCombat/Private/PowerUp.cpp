#include "PowerUp.h"
#include "Components/ShapeComponent.h"
#include "ReflectionUtilsFunctionLibrary.h"
#include "PowerUpComponent.h"

// Sets default values
APowerUp::APowerUp()
{
 	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APowerUp::BeginPlay()
{
	Super::BeginPlay();

	Collider = Cast<UShapeComponent>(GetComponentByClass(UShapeComponent::StaticClass()));
	if (!Collider)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not find a ShapeComponent in PowerUp components!"));
		ensure(false);
	}

	GetComponents<UPowerUpComponent>(PowerUpComponents);
	if (PowerUpComponents.Num() > 0)
	{
		Collider->OnComponentBeginOverlap.AddUniqueDynamic(this, &ThisClass::OnBeginOverlap);
	}
}

void APowerUp::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	for (const UPowerUpComponent* PowerUp : PowerUpComponents)
	{
		PowerUp->ActivatePowerUp(OtherActor);
	}
	Destroy();
}
