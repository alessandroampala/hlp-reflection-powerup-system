#include "PowerUpComponent.h"
#include "ReflectionUtilsFunctionLibrary.h"

UPowerUpComponent::UPowerUpComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UPowerUpComponent::ActivatePowerUp(AActor* TargetActor) const
{
	for (const FPowerUpModifier& Modifier : Modifiers)
	{
		ApplyModifier(TargetActor, Modifier);
	}
}

void UPowerUpComponent::ApplyModifier(AActor* TargetActor, const FPowerUpModifier& Modifier) const
{
	void* Object = nullptr;
	const FProperty* Property = UReflectionUtilsFunctionLibrary::RetrieveProperty(TargetActor, Modifier.PropertyPath, Object);
	if (Object && Property)
	{
		if (const FFloatProperty* FloatProperty = CastField<FFloatProperty>(Property))
		{
			const float Value = FloatProperty->GetPropertyValue_InContainer(Object);

			switch (Modifier.Type)
			{
			case EModifierType::Add:
				FloatProperty->SetPropertyValue_InContainer(Object, Value + Modifier.Value);
				break;
			case EModifierType::Multiply:
				FloatProperty->SetPropertyValue_InContainer(Object, Value * Modifier.Value);
				break;
			default:
				ensureMsgf(false, TEXT("This should never happen"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("[PowerUp] Modifier could not be applied because property %s is not FloatProperty."), *Modifier.PropertyPath);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("[PowerUp] Property %s not found."), *Modifier.PropertyPath);
	}
}
