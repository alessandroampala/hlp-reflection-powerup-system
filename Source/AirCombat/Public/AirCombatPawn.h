#pragma once

#include "CoreMinimal.h"
#include "Aircraft.h"
#include "AirCombatPawn.generated.h"

UCLASS()
class AIRCOMBAT_API AAirCombatPawn : public AAircraft
{
	GENERATED_BODY()

public:

	AAirCombatPawn();
};