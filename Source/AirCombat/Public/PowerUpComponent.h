#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "PowerUpComponent.generated.h"

UENUM()
enum class EModifierType : uint8
{
	Multiply,
	Add,
	COUNT		UMETA(Hidden),
};

USTRUCT(BlueprintType)
struct FPowerUpModifier
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString PropertyPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EModifierType Type;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AIRCOMBAT_API UPowerUpComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	TArray<FPowerUpModifier> Modifiers;

	UPowerUpComponent();

	UFUNCTION(BlueprintCallable)
	void ActivatePowerUp(AActor* TargetActor) const;

private:

	void ApplyModifier(AActor* TargetActor, const FPowerUpModifier& Modifier) const;
	
};
