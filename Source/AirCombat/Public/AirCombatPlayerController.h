#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AirCombatPlayerController.generated.h"

UCLASS()
class AIRCOMBAT_API AAirCombatPlayerController : public APlayerController
{
	GENERATED_BODY()
};