#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AirCombatGameMode.generated.h"

UCLASS()
class AIRCOMBAT_API AAirCombatGameMode : public AGameModeBase
{
	GENERATED_BODY()
};