

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PowerUp.generated.h"

class UShapeComponent;
class UPowerUpComponent;

UCLASS()
class AIRCOMBAT_API APowerUp : public AActor
{
	GENERATED_BODY()
	
public:	

	APowerUp();

protected:

	virtual void BeginPlay() override;

private:

	UPROPERTY(EditDefaultsOnly)
	float ColliderScale = 1;

	UPROPERTY()
	TObjectPtr<UShapeComponent> Collider;

	UPROPERTY()
	TArray<UPowerUpComponent*> PowerUpComponents;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
