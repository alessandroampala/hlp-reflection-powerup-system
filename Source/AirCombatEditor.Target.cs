// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class AirCombatEditorTarget : TargetRules
{
	public AirCombatEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "AirCombat" } );
		IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_1;
    }
}
