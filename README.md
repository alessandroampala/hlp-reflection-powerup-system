# HLP - Reflection PowerUp System
This is the project for the High Level Programming course of a Master in Game Development. Consists in the implementation of a PowerUp System through the Unreal Reflection System.

## Request
Starting from the base project "AirCombat", implement a completely *data-driven* PowerUp System.

A PowerUp must be an Actor with a *visual component* (a primitive or a billboard are enough), placed in arbitrary locations around the map. The player can activate a PowerUp by colliding with it.

The PowerUp must have an ActorComponent that allows a Game Designer to specify:
- a **query** of the property to modify;
- a **multiplier** to apply to that property.

The alteration of the property must be performed though **reflection**. This way it is possible to realize an arbitrary number of PowerUp, at the discretion of the Game Designer, using just one component.

## Implementation
There are two main classes:
- The Actor: `PowerUp`,
- The ActorComponent: `PowerUpComponent`.

### PowerUp Actor
In BeginPlay, this class retrieves all the `PowerUpComponent`s of the current actor and registers a delegate OnBeginOverlap of the first `ShapeComponent` found.

On overlap, the delegate is fired and all the PowerUps the components define are activated.

### PowerUp ActorComponent
A PowerUpComponent exposes an array of modifiers. A modifier is a unit of the PowerUp that defines an alteration of a property. It is made of:
- a **PropertyPath** string: the string that defines which property of which component to modify. It is in the form of `Component.Property`, or `This.Property` if the property to modify is located in the Actor class and not in one of its components.
- a **Value**
- a **Type**: can be `Multiply` or `Add`. The Value of the modifier will act as a multiplier in the first case and as an addend in the other.

This way it is possible to define a list of modifiers to create a single PowerUp, and to add multiple PowerUpComponents to logically separate each PowerUp.